import { useEffect, useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";

function App() {
  const [content, setContent] = useState(<></>);

  useEffect(() => {
    const time = setTimeout(() => {
      setContent(
        <input
          type="button"
          value={"click"}
          className="btn-click scale-up-center "
          />
      );
    }, 2000);
    return () => clearTimeout(time);
  }, []);
  return <div className="App">{content}</div>;
}

export default App;
